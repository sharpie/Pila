#include <iostream>
#include <string>
#include "Pila.h"

using namespace std;

Pila::Pila(int maximo) {
    this->maximo = maximo;
    tope = NULL;
}

void Pila::push(int numero) {
    nodo *Nuevo = new nodo;
    Nuevo->numero = numero;
    Nuevo->siguiente = NULL;
    if ( pila_vacia() ) {
        tope = Nuevo;
    } 
    else {
        Nuevo->siguiente = tope;
        tope = Nuevo;
    }
    contador_global += 1;
}

void Pila::pop() {
    if ( pila_vacia() ) {
        cout << "Error. No se puede hacer pop en pila vacia." << endl;
        return;
    }
    nodo *Objetivo = tope;
    tope = tope->siguiente;
    delete Objetivo;

    contador_global -= 1;
}

void Pila::mostrar() {
    nodo *Parcial = tope;
    const string separador  = "-----------------";

    cout << "\n\n\t\tDATOS EN PILA: \t\t\n\n";
    while ( Parcial != NULL ) {
        cout << separador << endl;
        cout << Parcial->numero << endl;
        Parcial = Parcial->siguiente;
    }
}

bool Pila::pila_llena() {
    return contador_global >= maximo;
}

bool Pila::pila_vacia() {
    return contador_global == 0;
}

int Pila::Tope() {
    return tope->numero;
}