struct nodo {
    int numero;
    nodo *siguiente;
};

class Pila {
    private:
        int maximo;
        int contador_global = 0;
        nodo *tope;
    public:
        Pila(int);
        void registrar();
        void push(int);
        void pop();
        void mostrar();
        bool pila_llena();
        bool pila_vacia();
        int Tope();
};