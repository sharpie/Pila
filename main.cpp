#include <iostream>
#include "Pila.h"

using namespace std;
    
const int MAX = 10; /* MAX = numero maximo de elementos en pila */
Pila Datos(MAX);
Pila Auxiliar(MAX);

void ascendente(Pila Input) {
    while ( !Input.pila_vacia() ) {
        int temp = Input.Tope();
        Input.pop();
        while ( !Auxiliar.pila_vacia() && Auxiliar.Tope() < temp ) {
            Input.push(Auxiliar.Tope());
            Auxiliar.pop();
        }
        Auxiliar.push(temp);
    }
    Auxiliar.mostrar();
}

int main() {
    int opc, numero;
    do {
        cout << "\n\nMenu\n\n[1] Registrar numero.\n[2] Eliminar numero.\n[3] Mostrar numero.\n[4] Mostrar pila en orden ascendente.\n[0] Salir.\n\n\n -> ";
        cin >> opc;
        switch(opc) {
            case 0: cout << "Saliendo..."; break;
            case 1: cout << "Ingrese numero: "; cin>>numero; Datos.push(numero); break;
            case 2: Datos.pop(); break;
            case 3: Datos.mostrar(); break;
            case 4: ascendente(Datos); break;
            default: break;
        }
    } while ( opc != 0 );
    return 0;
}